<?php if ($this->session->flashdata("confirmacion")) : ?>
  <script type="text/javascript">
    toastr.success("<?php echo $this->session->flashdata("confirmacion"); ?>");
  </script>
  <?php $this->session->set_flashdata("confirmacion", "")  ?>
<?php endif; ?>

<?php if ($this->session->flashdata("ERROR")) : ?>
  <script type="text/javascript">
    toastr.error("<?php echo $this->session->flashdata("ERROR"); ?>");
  </script>
  <?php $this->session->set_flashdata("ERROR", "")  ?>
<?php endif; ?>

<?php if ($this->session->flashdata("Bienvenida")) : ?>
  <script type="text/javascript">
    toastr.info("<?php echo $this->session->flashdata("Bienvenida"); ?>");
  </script>
  <?php $this->session->set_flashdata("Bienvenida", "")  ?>
<?php endif; ?>

<style>
    .tit {
        color: aliceblue;
    }

    .body {
        background-color: #E7FFFF;
    }

    .ed {
        color: blue;
    }

    .obligatorio {
        color: red;
        background-color: white;
        border-radius: 20px;
        font-size: 10px;
        padding-left: 5px;
        padding-right: 5px;
    }

    .error {
        color: red;
        font-weight: bold;
    }

    input.error {
        border: 2px solid red;
    }
</style>

<!-- Importando JQuerry -->
<script src="https://code.jquery.com/jquery-3.7.0.js" integrity="sha256-JlqSTELeR4TLqP0OG9dxM7yDPqX1ox/HfgiSLBj8+kM=" crossorigin="anonymous"></script>
<!-- Importando JQuerry Validate -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


</body>

</html>