<h1 Align="center">Listar Pacientes</h1>

<?php if ($pacientes) : ?>
    <table class="table table-striped text-center" id="tbl_Pacientes">
        <thead class="text-center">
            <tr>
                <th>CODIGO</th>
                <th>NOMBRE</th>
                <th>APELLIDO</th>
                <th>TIPO DE SANGRE</th>
                <th>GENERO</th>
                <th>PAIS</th>
                <th>FECHA DE NACIMIENTO</th>
                <th>FOTO</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($pacientes as $filaTemporal) : ?>
                <tr>
                    <td>
                        <?php echo $filaTemporal->egbc_codigo; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->egbc_nombre	; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->egbc_apellido; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->egbc_tip_sangre; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->egbc_genero; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->egbc_pais; ?>
                    </td>
                    <td>
                        <?php echo $filaTemporal->egbc_fech_naci; ?>
                    </td>

                    <td>
                        <?php if ($filaTemporal->egbc_foto_paci != "") : ?>
                            <a href="<?php echo base_url('uploads/') . $filaTemporal->egbc_foto_paci; ?>" target="_blank">
                                <img src="<?php echo base_url('uploads/') . $filaTemporal->egbc_foto_paci; ?>" alt="" width="50px">
                            </a>
                        <?php else : ?>
                            <img src="<?php echo base_url('assets/images/sinImagen.png') ?>" alt="">
                        <?php endif; ?>
                    </td>
                    <td>
                    <a href="<?php echo site_url(); ?>/pacientes/editarPaciente/<?php echo $filaTemporal->egbc_codigo; ?>" title="Editar Paciente">
                            <i><img src="<?php echo base_url('assets/images/pencil.png')?>" alt="">Editar</i>
                        </a>
                        &nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url(); ?>/Pacientes/eliminaPac/<?php echo $filaTemporal->egbc_codigo; ?>" title="Eliminar Paciente">
                            <i><img src="<?php echo base_url('assets/images/trash.png') ?>" alt="">Eliminar</i>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else : ?>
    <h1>NO POSEE PacienteS ._.</h1>
<?php endif; ?>

<script type="text/javascript">
    $("#tbl_Pacientes").DataTable();
</script>