<h1 Align="center">Agregar Nuevo Paciente</h1>

<form class="" id="frm_nuevo_Paciente" action="<?php echo site_url(); ?>/Pacientes/editPac" method="post" enctype="multipart/form-data">
    <div class="container">
        <div class="row">
        <input type="text" class="form-control" name="egbc_codigo" id="egbc_codigo" hidden value="<?php echo $editaPaci->egbc_codigo; ?>">
            <div class="col-md-4">
                <label for="">Nombre: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar nombre del Paciente" class="form-control" name="egbc_nombre" required value="<?php echo $editaPaci->egbc_nombre; ?>">
            </div>
            <div class="col-md-4">
                <label for=""> Apellido: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar el Apellido del Paciente" class="form-control" required name="egbc_apellido" value="<?php echo $editaPaci->egbc_apellido; ?>">
                <br>
            </div>

            <div class="col-md-4">
                <label for="egbc_tip_sangre">Tipo de Sangre: </label>
                <select name="egbc_tip_sangre" class="form-control" id="egbc_tip_sangre" required value="<?php echo $editaPaci->egbc_tip_sangre; ?>">
                    <option value="A+">A+</option>
                    <option value="A-">A-</option>
                    <option value="O+">O+</option>
                    <option value="O-">O-</option>
                    <option value="B+">B+</option>
                    <option value="B-">B-</option>

                </select>
            </div>
            
            <div class="col-md-4">
                <label for="egbc_genero">Genero: </label>
                <select name="egbc_genero" class="form-control" id="egbc_genero" required value="<?php echo $editaPaci->egbc_genero; ?>">
                    <option value="Hombre">Hombre</option>
                    <option value="Mujer">Mujer</option>
                </select>
            </div>

            <div class="col-md-4">
                <label for=""> Pais: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="text" placeholder="Ingresar el Pais del Paciente" class="form-control" required name="egbc_pais" value="<?php echo $editaPaci->egbc_pais; ?>">
                <br>
            </div>

            <div class="col-md-4">
                <label for=""> Fecha de Nacimiento: <span class="obligatorio">(Campo Requerido)</span></label>
                <br>
                <input type="date" class="form-control" required name="egbc_fech_naci" value="<?php echo $editaPaci->egbc_fech_naci; ?>">
                <br>
            </div>
            
            

            <div class="row" >
                <div class="col-md-2">

                </div>
                <div class="col-md-8">
                    <label for=""> Foto:</label>
                    <br>
                    <input type="file" name="egbc_foto_paci" id="egbc_foto_paci" >
                    <br>
                </div>
                <div class="col-md-2">

                </div>
            </div>

            <br>
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary">
                    GUARDAR CAMBIOS
                </button>
                &nbsp;
                <a href="<?php echo site_url(); ?>/pacientes/listPac" class="btn btn-danger">CANCELAR</a>
            </div>

</form>

<script type="text/javascript">
    $("#frm_nuevo_Paciente").validate({
        rules: {
            egbc_nombre: {
                required: true,
                minlength: 3,
                maxlength: 30,
                letras: true,
            },
            egbc_apellido: {
                required: true,
                minlength: 5,
                maxlength: 20,
                letras: true,
            },
            
            
            egbc_pais: {
                required: true,
                minlength: 5,
                maxlength: 100,
                letras: true,
            },
            egbc_fech_naci: {
                required: true,
                date: true,
            },
            

        },
        messages: {
            egbc_nombre: {
                required: "Por favor ingresar un nombre",
                minlength: "El nombre debe tener al menos 3 caracteres",
                maxlength: "Nombre incorrecto",

            },
            egbc_apellido: {
                required: "Por favor ingrese el Apellido del Paciente",
                minlength: "El Apellido debe tener al menos 3 digito",
                maxlength: "Apellido Incorrecta",
                
            },
            egbc_pais: {
                required: "Por favor ingresar un nombre",
                minlength: "El nombre debe tener al menos 3 caracteres",
                maxlength: "Nombre incorrecto",

            },
            
            
        }
    });
</script>

<script type="text/javascript">
  $("#egbc_foto_paci").fileinput({
    language: 'es'
  });
</script>