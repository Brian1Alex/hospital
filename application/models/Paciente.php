<?php
class Paciente extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function insertar($datos)
    {
        return $this->db->insert("paciente", $datos);
    }

    function borrar($egbc_codigo)
        {
            $this->db->where("egbc_codigo", $egbc_codigo);
            return $this->db->delete("paciente");
        }

    function obtenerPac()
    {
        $listPac = $this->db->get("paciente");
        if ($listPac->num_rows() > 0) {
            return $listPac->result();
        } else {
            return false;
        }
    }


    function editar($egbc_codigo, $dat)
        {
            $this->db->where("egbc_codigo", $egbc_codigo);
            return $this->db->update('paciente', $dat);
        }

        function obtenerPorId($egbc_codigo)
        {
            $this->db->where("egbc_codigo", $egbc_codigo);
            $paciente = $this->db->get("paciente");
            if ($paciente->num_rows() > 0) {
                return $paciente->row();
            }
            return false;
        }

}
