<?php
class Pacientes extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Paciente');
    }

    public function nuePac()
    {
        $this->load->view('header');
        $this->load->view('pacientes/nuePac');
        $this->load->view('footer');
    }

    public function listPac()
    {
        $data['pacientes'] = $this->Paciente->obtenerPac();
        $this->load->view('header');
        $this->load->view('pacientes/listPac', $data);
        $this->load->view('footer');
    }

    public function editarPaciente($egbc_codigo)
    {
        $dat['editaPaci'] = $this->Paciente->obtenerPorId($egbc_codigo);
        $this->load->view('header');
        $this->load->view('pacientes/editarPaciente', $dat);
        $this->load->view('footer');
    }



    public function guardaPac()
    {
        $datosNuePac = array(
            "egbc_nombre" => $this->input->post('egbc_nombre'),
            "egbc_apellido" => $this->input->post('egbc_apellido'),
            "egbc_tip_sangre" => $this->input->post('egbc_tip_sangre'),
            "egbc_genero" => $this->input->post('egbc_genero'),
            "egbc_pais" => $this->input->post('egbc_pais'),
            "egbc_fech_naci" => $this->input->post('egbc_fech_naci')
        );
        $this->load->library("upload");
        $new_name = "foto_paci_" . time() . "_" . rand(1, 5000);
        $config['file_name'] = $new_name . '_1';
        $config['upload_path']          = FCPATH . 'uploads/';
        $config['allowed_types']        = 'jpeg|jpg|png|.png';
        $config['max_size']             = 1024 * 5;
        $this->upload->initialize($config);

        if ($this->upload->do_upload("egbc_foto_paci")) {
            $dataSubida = $this->upload->data();
            $datosNuePac["egbc_foto_paci"] = $dataSubida['file_name'];
        }

        if ($this->Paciente->insertar($datosNuePac)) {
            $this->session->set_flashdata("confirmacion", "Paciente guardado con EXITO!!!");
        } else {
            "<h1>Error al ingresar Datos :(</h1>";
        }
        redirect('pacientes/listPac');
    }

    public function eliminaPac($egbc_codigo)
    {
        if ($this->Paciente->borrar($egbc_codigo)) {
            $this->session->set_flashdata("confirmacion", "Paciente Eliminado con EXITO!!!");
            
        } else {
            "<h1>Error al eliminar ._.</h1>";
        }
        redirect('pacientes/listPac');
    }

    public function editPac()
    {
        $datosEditados = array(
            "egbc_nombre" => $this->input->post('egbc_nombre'),
            "egbc_apellido" => $this->input->post('egbc_apellido'),
            "egbc_tip_sangre" => $this->input->post('egbc_tip_sangre'),
            "egbc_genero" => $this->input->post('egbc_genero'),
            "egbc_pais" => $this->input->post('egbc_pais'),
            "egbc_fech_naci" => $this->input->post('egbc_fech_naci')
        );
        $this->load->library("upload");
        $new_name = "foto_pla_" . time() . "_" . rand(1, 5000);
        $config['file_name'] = $new_name . '_1';
        $config['upload_path']          = FCPATH . 'uploads/';
        $config['allowed_types']        = 'jpeg|jpg|png|.png';
        $config['max_size']             = 1024 * 5;
        $this->upload->initialize($config);

        if ($this->upload->do_upload("egbc_foto_paci")) {
            $dataReSubida = $this->upload->data();
            $datosEditados["egbc_foto_paci"] = $dataReSubida['file_name'];
        }


        $egbc_codigo = $this->input->post("egbc_codigo");
        if ($this->Paciente->editar($egbc_codigo, $datosEditados)) {
            $this->session->set_flashdata("confirmacion", "PacienteEDITADO con EXITO!!!");
            redirect('pacientes/listPac');
        }
    }
}
